###########################

The folder "a51" contains the code used for the experiments concerning A5/1 reported in Section 3.4 of the paper
The folder "grain" contains the code used for the experiments concerning grain reported in Section 4.3 of the paper

To compile the a51 experiences:
    $ make
Note that by default every experiences are commented in a51/main.cpp and will need to be de-commented to run them

To compile the grain experience:
    $ make grain

###########################

In the a51 folder,
    the "distribution" files correspond to the test of the balanced characteristic of the boolean function that
    takes the 33-bit CP part of an A5/1 internal state to output the 5 corresponding bits of keystream; 
    the "experience" files correspond to the test of Algorithm 3;

The grain folder follows the same organization than the a51 folder.

###########################

plotter.py is the python3 script we use to generate the histograms in the paper

The command to generate the histogram for A5/1 (in the example, for 54 bits of key) is:
    $ python3 plotter.py a51 exp54.csp
where exp54.csp is generated from the function compute.csp

The command to generate the histogram for grain is:
    $ python3 plotter.py grain expgrain.output
where expgrain.output is only the terminal output of ./exp_grain without its first line;
