#include "distribution.hpp"

/*
    misc
*/
void printVec(std::vector<bool> const& input) {
    std::cout << "vector is: ";
    for(auto value : input) {
        std::cout << (int)value;
    }
    std::cout << std::endl;
}

/*
    misc
*/
unsigned long long int vectorToInt(std::vector<bool> input) {
    unsigned long long int result = 0;
    for(int i = input.size() - 1; i >= 0; i--) {
        result *= 2;
        result += (int)input[i];
    }
    return result;
}

/*
    misc
*/
std::vector<bool> intToVector(unsigned long long int input) {
    std::vector<bool> result(33, 0);
    int current = input;
    for(int i = 0; i < 33; i++) {
        result[i] = (bool)(current%2);
        current /= 2;
    }
    return result;
}

/*
    compute a boolean vector correponding to the 5-bit of prefixe generated from a state where the 33-bit CP part is known
*/
std::vector<bool> computePrefix33(unsigned long long int pState, bool verbose) {

    std::vector<bool> pStateVec = intToVector(pState);
    std::vector<bool> prefix(5, false);

    int head1 = 0, head2 = 0, head3 = 0;
    int clock1= 6, clock2= 6, clock3= 6;

    //intial partial state
    std::vector<bool> lfsr1(11, false);
    std::vector<bool> lfsr2(11, false);
    std::vector<bool> lfsr3(11, false);
    for(int i = 0; i < 11; i++) {
        lfsr1[i] = pStateVec[3*i];
        lfsr2[i] = pStateVec[3*i + 1];
        lfsr3[i] = pStateVec[3*i + 2];
    }

    if(verbose) {
        std::cout << "lfsr are: " << std::endl;
        printVec(lfsr1);
        printVec(lfsr2);
        printVec(lfsr3);
    }

    // compute clock than bit of prefix
    for(int round = 0; round < 5; round++) {
        //clock
        bool maj = (lfsr1[clock1] & lfsr2[clock2]) ^ (lfsr1[clock1] & lfsr3[clock3]) ^ (lfsr2[clock2] & lfsr3[clock3]);
        if(!(maj ^ lfsr1[clock1])) {
            clock1++;
            head1++;
        }
        if(!(maj ^ lfsr2[clock2])) {
            clock2++;
            head2++;
        }
        if(!(maj ^ lfsr3[clock3])) {
            clock3++;
            head3++;
        }
        //prefix bit
        prefix[round] = lfsr1[head1] ^ lfsr2[head2] ^ lfsr3[head3];
    }
    return prefix;
}


void distribution() {
    //initialization of distribution of prefix
    std::vector<int> distrib(1<<5, 0);

    //compute prefix
    for(unsigned long long int partialState = 0; partialState < (1ULL<<33); partialState++) {
        std::vector<bool> currentPrefix = computePrefix33(partialState, false);
        int index = vectorToInt(currentPrefix);
        if(index <0 || index >= (1<<5)) {
            std::cout << "bad index for " << partialState << std::endl;
        } else {
            distrib[index]++;
        }
    }

    //print of distribution of prefix
    std::cout << "dstribution of 5-bits prefixes over the 33 bits of partial states:" << std::endl;
    for(int i=0; i < (1<<5); i++) {
        std::cout << "for prefix " << std::bitset<5>(i) << " number of partial states is: " << distrib[i] << std::endl;
    }
}
