#include "distribution.hpp"
#include "experience.hpp"

/*
    function to load from the binary saves of the experience and generate the .csp use to plot figure 3.
*/
void computecsp(std::string fileName) {
    std::vector<uint64_t> csp(1<<8, 0LL);

    std::cout << "loading " + fileName + " and creating .csp"<< std::endl;
    std::ifstream l(fileName.c_str(), std::ios::in);
    for(uint64_t i = 0LL; i < 1LL <<33; i++) {
        uint8_t inter;
        l.read((char*)&inter, sizeof(uint8_t));
        csp[inter]++;
    }
    std::cout << "finish loading and .csp; beginning save" << std::endl;

    std::string cspName = fileName.replace(fileName.size()-4, 4, ".csp");
    char sep[] = {',', ' '};
    std::ofstream f(cspName.c_str(), std::ios::out | std::ios::trunc);
    for(int iter = 0; iter < 1 << 8; iter++){
        f << csp[iter];
        f.write(sep, 2*sizeof(char));
    }

    std::cout << "to obtain the graphic run: python3 plotter.py a51 " + fileName << std::endl;
}

int main(int argc, char *argv[]) {
    /*
        experience to check that the boolean function corresponding to the generation of 5 keystream
        bits from the CP part of a A5/1 internal state is balanced
    */
    // distribution();

    /*
        direct generation of the 33-bit CP part from AES-CTR
    */
    // exp(1LL << 36, true, true);
    // computecsp("res/expneutral.bin");

    /*
        experience corresponding to Algorithm  with 64-bit key
    */
    // exp(1LL << 36, true, false);
    // computecsp("res/exp64.bin");

    /*
        experience corresponding to Algorithm
    */
    // exp(1LL << 36, false, false);
    // computecsp("res/exp54.bin");

    /*
        new experiences;
    */
    newExp(268435456); // 2^28
}
