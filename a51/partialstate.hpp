#ifndef _PARTIALSTATE_HPP_
#define _PARTIALSTATE_HPP_

#include <iostream>
#include <bitset>
#include <vector>


class PartialState {
public:
    //attributs
    uint64_t value; //64-bit value of the internal state
    uint64_t known; //64-bit to described if the value of the corresponding bit of the internal state is known;

    int headLFSR1, headLFSR2, headLFSR3; // position of the output of the 3 LFSRs for clocking in place
    int clockLFSR1, clockLFSR2, clockLFSR3; // position of the clocking taps of the LFSR for the clocking in place

    //constructors
    PartialState();
    PartialState(uint64_t value);
    PartialState(uint64_t value, uint64_t known);

    //other methods
    /*
        misc
    */
    void printPartialState();

    /*
        method to give the index of every unknown bits in a partially known LFSR
    */
    std::vector<int> giveUnknown();

    /*

    */
    void clockingRegular(bool async, bool bitToAdd);

    /*

    */
    void clockingInPlace(bool async);


    /*

    */
    uint64_t computePrefixBit();


    /*

    */
    uint64_t computePrefix(int sizeOfPrefix);


    /*

    */
    void loadBit(int sizeOfLoad, uint64_t Load, int position);

    /*
        
    */
    void load(std::vector<int> sizeOfLoad, std::vector<uint64_t> Load, std::vector<int> positions);


    /*
        the full initialization of the A5/1 cipher
    */
    void initialization(uint64_t key, uint64_t frame);
};

#endif
