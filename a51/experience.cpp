#include "experience.hpp"
#include "aesni.hpp"

#include <cstdlib>
#include <ctime>
#include <fstream>


/**************************************************************************************************/
/*                                          program                                               */
/**************************************************************************************************/

uint64_t select33bits(const PartialState& state) {
    return ((state.value >>  4) & 0x1FULL)        ^ (((state.value >> 25) & 0x1FULL) <<  5) ^ (((state.value >> 47) & 0x1FULL) << 10)
        ^ (((state.value >> 13) & 0x2FULL) << 15) ^ (((state.value >> 35) & 0x3FULL) << 21) ^ (((state.value >> 58) & 0x3FULL) << 27);
}


void exp(uint64_t nbOfExp, bool keybits64, bool neutral) {
    std::srand(time(0));
    std::vector<uint8_t> configuration(1ULL<<33, 0);
    uint64_t keyExp, frameExp;

    /*
     *      generating initial key and counter for aes-ctr-drbg
     */
    uint8_t key[16];
    std::cout << "key128: ";
    for(int i = 0; i < 16; i++) {
        key[i] = rand();
        // std::cout << std::bitset<8>(key[i]); // to print the key
    }
    std::cout << std::endl;
    __m128i key_schedule[20];
    aes128_load_key(key, key_schedule);

    __m128i ctr = {0, 0};
    uint8_t plain[16];
    uint8_t cipher[16];

    /*
     *      repeting experiences
     */
    for(uint64_t i = 0; i < nbOfExp; i++) {
        PartialState state = PartialState(0ULL);
        // if(i%(1<<30) == 0) std::cout << "has begun experience " << i << std::endl;

        /*
            obtaining the key and frame for this experiences
        */
        _mm_storeu_si128((__m128i *) plain, ctr++);
        aes128_enc(key_schedule, plain, cipher);

        if(neutral) state.value = *(uint64_t *)cipher;
        else {
            keyExp = *(uint64_t *)cipher;
            if(!keybits64) keyExp <<= 10;
            frameExp = (*(uint64_t *)(cipher + 8)) & 0x3FFFFF;

            /*
                A5/1 initialization
            */    for(uint64_t i = 0; i < (1ULL<<33); i++) {

    }
            // std::cout << "keyExp:   " << std::bitset<64>(keyExp) << std::endl << "frameExp: " << std::bitset<23>(frameExp) << std::endl;
            state.initialization(keyExp, frameExp);

        }

        // stocking result
        // std::cout << std::bitset<33>(select33bits(state)) << std::endl;
        configuration[select33bits(state)]++;
    }

    /*
        printing the data resulting from the experience directly in the terminal
    */
    // std::cout << "after " << nbOfExp << " this is the result distribution for tail: " << std::endl;
    // for(uint64_t i = 0; i < (1ULL<<33); i++) {
    //     if(configuration[i] != 0) {
    //         std::cout << "    " << std::bitset<33>(i) << "  -->  " << configuration[i] << std::endl;
    //     }
    // }


    /*
        saving the data as a binary file
    */
    // std::cout << "beginning save" << std::endl;
    std::string fileName;
    if(neutral) fileName = std::string("res/exp") + std::string("neutral") + std::string(".bin");
    else fileName = std::string("res/exp") + (keybits64?std::string("64"):std::string("54")) + std::string(".bin");

    std::ofstream f(fileName.c_str(), std::ios::out | std::ios::trunc | std::ios::binary);
    for(uint64_t iter = 0; iter < 1LL << 33; iter++){
        f.write((char*) &configuration[iter], sizeof(uint8_t));
    }
    configuration.clear();
}


int dichotomy(std::vector<uint>& indexMax, std::vector<uint32_t> const& configuration, uint32_t value) {
    int first = 0, last = indexMax.size()-1, result = 0;
    while(first < last) {
        result = (first + last)/2;
        if(configuration[indexMax[result]] == value) {
            return result;
        } else if (configuration[indexMax[result]] > value) {
            first = result + 1;
        } else {
            last = result - 1;
        }
    }
    return result;
}

void newExp(int numberOfSamples) {
    /*
        generation of master key for randomness generation
    */
    std::srand(time(0));
    uint8_t masterKey[16];
    for(int i = 0; i < 16; i++) masterKey[i] = rand();
    __m128i key_schedule[20];
    aes128_load_key(masterKey, key_schedule);

    /*
        generation of 5 random keystream values
    */
    __m128i ctr = {0, 0};
    uint8_t plain[16], cipher[16];
    _mm_storeu_si128((__m128i *) plain, ctr++);
    aes128_enc(key_schedule, plain, cipher);
    uint8_t targetKeystream = cipher[0] & (0x1F);

    /*
        the set X is generated; here, we will take the 2^16.6 values that are the most probable;
    */
    std::vector<uint32_t> configuration(1ULL<<33, 0);
    std::vector<uint> indexMax(99335,0); // 2^16.6 values;

    /*
        the experiences in itself;
    */
    uint64_t key, frame;
    for(int samplingNum = 0; samplingNum < numberOfSamples; samplingNum++) {
        bool flag = false;
        PartialState state = PartialState(0ULL);
        do {
            /*
                randomly generate key and frame until finding acceptable value of keystream
            */
            _mm_storeu_si128((__m128i *) plain, ctr++);
            aes128_enc(key_schedule, plain, cipher);
            key = *(uint64_t *)cipher;
            frame = (*(uint64_t *)(cipher + 8)) & 0x3FFFFF;

            /*
                compute the partial state from the key and frame
            */
            state = PartialState(0ULL);
            state.initialization(key, frame);
            uint8_t generatedKeystream = state.computePrefix(5);
            if(generatedKeystream == targetKeystream) flag = true;
        } while(!flag);
        configuration[select33bits(state)]++;
    }

    double prob = 0;

    for(uint64_t i = 0; i < (1ULL<<33); i++) {
        if(configuration[i] != 0 && configuration[i] > indexMax[99334]) {
            int indexInsertion = dichotomy(indexMax, configuration, configuration[i]);
            for(int j = indexMax.size()-1; j > indexInsertion; j--) {
                indexMax[j] = indexMax[j-1];
            }
            indexMax[indexInsertion] = i;
        }
    }
    for(auto& iter : indexMax) {
        prob += configuration[iter];
    }
    prob /= numberOfSamples;

    std::cout << " The computed probability is " << prob << std::endl;
}
