#include "partialstate.hpp"

/*

*/
PartialState::PartialState() : value(0ULL), known(0ULL), headLFSR1(18), headLFSR2(40), headLFSR3(63),
                                clockLFSR1(8), clockLFSR2(29), clockLFSR3(51) {}
PartialState::PartialState(uint64_t value) : value(value), known(~0ULL), headLFSR1(18), headLFSR2(40), headLFSR3(63),
                                clockLFSR1(8), clockLFSR2(29), clockLFSR3(51) {}
PartialState::PartialState(uint64_t value, uint64_t known) : value(value), known(known), headLFSR1(18), headLFSR2(40), headLFSR3(63),
                                clockLFSR1(8), clockLFSR2(29), clockLFSR3(51) {}


/*

*/
void PartialState::printPartialState() {
    std::cout << "    ";
    for(int bit = 18; bit >= 0; bit--) {
        if((known >> bit) & 1ULL) {
            std::cout << ((value >> bit) & 1ULL);
        } else {
            std::cout << "*";
        }
    }
    std::cout << std::endl << " ";
    for(int bit = 21; bit >= 0; bit--) {
        if((known >> (bit + 19)) & 1ULL) {
            std::cout << ((value >> (bit + 19)) & 1ULL);
        } else {
            std::cout << "*";
        }
    }
    std::cout << std::endl;
    for(int bit = 22; bit >= 0; bit--) {
        if((known >> (bit + 41)) & 1ULL) {
            std::cout << ((value >> (bit + 41)) & 1ULL);
        } else {
            std::cout << "*";
        }
    }
    std::cout << std::endl;
}

/*

*/
void PartialState::clockingRegular(bool async, bool bitToAdd){
    uint64_t maj = (((value >> clockLFSR1) & 1ULL) + ((value >> clockLFSR2) & 1ULL) + ((value >> clockLFSR3) & 1ULL))/2;
    if(!async || ((value >> clockLFSR1) & 1ULL) == maj) {
        value = (~0x7FFFFULL & value) ^ (( value << 1) & 0x7FFFFULL) ^
            (1ULL & (bitToAdd ^ (value >> 13) ^ (value >> 16) ^ (value >> 17) ^ (value >> 18)));
        known = (~0x7FFFFULL & known) // valeur known qui ne concerne pas le premier lfsr
            ^ (( known << 1) & 0x7FFFFULL) //le shift des valeur sur ce lfsr
            ^ ((known >> 13) & (known >> 16) & (known >> 17) & (known >> 18) & 1ULL); //la nouveULLe valeur est connue si toute les valeurs pour la calculer sont connues
    }
    if(!async || ((value >> clockLFSR2) & 1ULL) == maj) {
        value = (~(0x3FFFFFULL << 19) & value) ^ (( value << 1) & (0x3FFFFEULL << 19)) ^
            ((1ULL & (bitToAdd ^ (value >> 40) ^ (value >> 39))) << 19);
        known = (~(0x3FFFFFULL << 19) & known) //autre lfsr
            ^ (( known << 1) & (0x3FFFFEULL << 19)) //on shift
            ^ ((1ULL & ((known >> 40) & (known >> 39))) << 19); // la nouveULLe valeur est connue si...
    }
    if(!async || ((value >> clockLFSR3) & 1ULL) == maj) {
        value = (~(0x7FFFFFULL << 41) & value) ^ (( value << 1) & (0x7FFFFEULL << 41)) ^
            ((1ULL & (bitToAdd ^ (value >> 63) ^ (value >> 62) ^ (value >> 61) ^ (value >> 48))) << 41);
        known = (~(0x7FFFFFULL << 41) & known)
            ^ (( known << 1) & (0x7FFFFEULL << 41))
            ^ ((1ULL & ((known >> 63) & (known >> 62) & (known >> 61) & (known >> 48))) << 41);
    }
}

/*

*/
void PartialState::clockingInPlace(bool async) {
    uint64_t maj = (((value >> clockLFSR1) & 1ULL) + ((value >> clockLFSR2) & 1ULL) + ((value >> clockLFSR3) & 1ULL))/2;
    if(!async || ((value >> clockLFSR1) & 1ULL) == maj) {
        headLFSR1--;
        clockLFSR1--;
    }
    if(!async || ((value >> clockLFSR2) & 1ULL) == maj) {
        headLFSR2--;
        clockLFSR2--;
    }
    if(!async || ((value >> clockLFSR3) & 1ULL) == maj) {
        headLFSR3--;
        clockLFSR3--;
    }
}

/*

*/
uint64_t PartialState::computePrefixBit() {
    return 1ULL & ((value >> headLFSR1) ^ (value >> headLFSR2) ^ (value >> headLFSR3));
}

/*

*/
uint64_t PartialState::computePrefix(int sizeOfPrefix) {
    uint64_t prefix = 0;

    for(int iteration = 0; iteration < sizeOfPrefix; iteration++){
        if((known >> clockLFSR1 & 1ULL) && (known >> clockLFSR2 & 1ULL) && (known >> clockLFSR3 & 1ULL)) {
            clockingInPlace(true);
        } else {
            std::cout << "could not compute " << iteration << "th bits of the prefix from unknown clock values in partialState" << std::endl;
            break;
        }
        if((known >> headLFSR1 & 1ULL) && (known >> headLFSR2 & 1ULL) && (known >> headLFSR3 & 1ULL)) {
            prefix ^= (computePrefixBit() << iteration);
        } else {
            std::cout << "could not compute bit " << iteration << " of the prefix from unknown head values in partialState" << std::endl;
            break;
        }
    }

    return prefix;
}

/*

*/
void PartialState::loadBit(int sizeOfLoad, uint64_t Load, int position) {
    uint64_t one = 0;
    for(int i=0; i < sizeOfLoad; i++) one ^= 1ULL << i;

    value = (value & ~(one << (position - sizeOfLoad + 1))) ^ (Load << (position - sizeOfLoad + 1));
    known |= one << (position - sizeOfLoad + 1);
}

/*

*/
void PartialState::load(std::vector<int> sizeOfLoad, std::vector<uint64_t> Load, std::vector<int> positions) {
    for(int i = 0; i < (int)sizeOfLoad.size(); i++) loadBit(sizeOfLoad[i], Load[i], positions[i]);
}


/*

*/
void PartialState::initialization(uint64_t key, uint64_t frame) {
    //loading key
    for(int i = 0; i < 64; i++) {
        clockingRegular(false, ((key >> i & 0x1ULL) != 0ULL));
    }
    //loading frame number
    for(int i = 0; i < 22; i++) {
        clockingRegular(false, ((frame >> i & 0x1ULL) != 0ULL));
    }
    // white rounds
    for(int i = 0; i < 100; i++) {
        clockingRegular(true, false);
    }
}
