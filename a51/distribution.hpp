#ifndef _DISTRIBUTION_HPP_
#define _DISTRIBTUION_HPP_

#include <cstdio>
#include <fstream>
#include <string>
#include <iostream>
#include <cstdlib>
#include <bitset>
#include <vector>
#include <map>


/*
    misc
*/
void printVectorTest(std::vector<bool> currentPrefix, bool verbose);

/*
    function to run the experience verifying the 5-bit prefix are equipobably distibuted over the 33-bit CP part possible;
*/
void distribution();

#endif
