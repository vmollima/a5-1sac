#ifndef _EXPERIENCE_HPP_
#define _EXPERIENCE_HPP_

#include "partialstate.hpp"

/*
    select the 33-bit part from a A5/1 internal state and stock it in a 64-bit unsigned integer;
*/
uint64_t select33bits(const PartialState& state);

/*
    run the experience described in Algorithm 3
    the boolean variable keybits64 correspond to the selection of 64-bit or 54-bit keys for the experiences
    to form the two red histograms of Figure 3
    the boolean variable neutral correspond to the direct generation (from AES-CTR) of the 33-bit CP part
    to form the blue histogram of Figure 3
*/
void exp(uint64_t nbOfExp, bool keybits64, bool neutral);


int dichotomy(std::vector<uint>& indexMax, std::vector<uint32_t> const& configuration, uint32_t value);

/*
    the new experiences
*/
void newExp(int numberOfSamples);

#endif
