import matplotlib.pyplot as plt
import numpy as np
import math
import sys

def grain():
    fileName = sys.argv[2]

    file = open(fileName, "r")
    contents = file.read()
    print(contents)
    histogram = contents.split("\n")
    print(histogram[0:6])
    histogram = [x[12:-5] for x in histogram]
    print(histogram[0:6])
    histogram = histogram[0:-1]

    histogram = [int(x[23:]) for x in histogram]
    print(histogram[0:6])

    X = np.arange(1024)
    fig, ax = plt.subplots()
    ax.bar(X, histogram, color='r', width = 0.5)
    plt.show()


def a51():
    fileName1 = sys.argv[2]

    ## file 1
    file = open(fileName1, "r")
    contents = file.read()
    # print(contents)
    histogram1 = contents.split(", ")
    histogram1.remove('')
    ctr = 0
    for i in histogram1:
        ctr += int(i)*histogram1.index(i)
    histogram1 = [math.log2(int(i)+1) for i in histogram1]
    # print(histogram1)
    print("in total, there are " + str(ctr) + " solution in histogram1")

    ## plotting
    fig, ax = plt.subplots()
    plt.xticks(fontsize=15)    # fontsize of the tick labels
    plt.yticks(fontsize=15)

    plt.xlabel('x: # of times reached', fontsize=15)
    plt.ylabel('y: # of 33-bit CP-parts (log scale)', fontsize=15)

    X = np.arange(256)
    ax.bar(X, histogram1, width=0.3, color='b', align='center')
    ax.set_xlim(-0.7,70)
    legend = plt.legend(title='random states')
    # plt.rcParams['title_fontsize'] = 20
    plt.setp(legend.get_title(),fontsize=15)

    # plt.tight_layout()
    plt.show()


if __name__ == "__main__":
    input = sys.argv[1]
    if(input == "a51"):
        a51()
    else:
        grain()
