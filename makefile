CC= g++
CFLAGS= -g -O3 -Wall -Weffc++ -std=c++11 -msse2 -msse -march=native -maes -o
RCFLAGS= -fopenmp -lpthread

a51: a51/partialstate.cpp a51/distribution.cpp a51/experience.cpp a51/main.cpp
	$(CC) $(CFLAGS) exp_a51 a51/partialstate.cpp a51/distribution.cpp a51/experience.cpp a51/main.cpp $(RCFLAGS)

grain: grain/partialstate.cpp grain/experience.cpp grain/main.cpp
	$(CC) $(CFLAGS) exp_grain grain/partialstate.cpp grain/experience.cpp grain/main.cpp $(RCFLAGS)

clean:
	rm -f prog *.o
