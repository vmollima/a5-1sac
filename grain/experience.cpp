#include "experience.hpp"
#include "aesni.hpp"

#include <cstdlib>
#include <ctime>
#include <fstream>
#include <map>

using namespace::std;

/**************************************************************************************************/
/*                                          program                                               */
/**************************************************************************************************/

int selectTargetBit(PartialState state) {
    return ((state.lfsr0 >> 3) & 1LL)
    ^ (((state.lfsr0 >> 25) &  1LL) << 1)
    ^ (((state.lfsr0 >> 46) &  1LL) << 2)
    ^ (((state.lfsr1 >>  0) &  1LL) << 3)
    ^ (((state.nfsr0 >> 63) &  1LL) << 4);
}

int selectTargetBitExtend(PartialState state) {
    return ((state.lfsr0 >> 3) & 1LL)
    ^ (((state.lfsr0 >> 25) &  1LL) << 1)
    ^ (((state.lfsr0 >> 46) &  1LL) << 2)
    ^ (((state.lfsr1 >>  0) &  1LL) << 3)
    ^ (((state.nfsr0 >> 63) &  1LL) << 4)
    ^ (((
        (state.nfsr0 >>  1) ^ (state.nfsr0 >>  2) ^ (state.nfsr0 >>  4) ^(state.nfsr0 >> 10) ^ (state.nfsr0 >> 31) ^ (state.nfsr0 >> 43) ^ (state.nfsr0 >> 56)
    ) & 1) << 5);
}

void exp(uint64_t nbOfExp) {
    std::srand(time(0));
    std::vector<uint64_t> configuration(1<<10, 0);
    uint64_t keyExp0, keyExp1, ivExp;

    /*
     *      generating initial key and counter for aes-ctr-drbg
     */
    uint8_t key[16];
    // std::cout << "key128: ";
    for(int i = 0; i < 16; i++) {
        key[i] = rand();
        // std::cout << std::bitset<8>(key[i]);
    }
    // std::cout << std::endl;
    __m128i key_schedule[20];
    aes128_load_key(key, key_schedule);

    __m128i ctr = {0, 0};
    uint8_t plain[16];
    uint8_t cipher[16];

    /*
     *      repeting experiences
     */
    for(uint64_t i = 0; i < nbOfExp; i++) {
        PartialState state = PartialState();
        // if(i%(1<<15) == 0) std::cout << "beginning experience " << i << std::endl;

        /*
            generation of the key and the iv for this iteration of the experience
        */
        _mm_storeu_si128((__m128i *) plain, ctr+=1);
        aes128_enc(key_schedule, plain, cipher);
        keyExp0 = *(uint64_t *)cipher;
        ivExp = (*(uint64_t *)(cipher + 8));
        // std::cout << "ivExp: " << std::bitset<64>(ivExp) << std::endl;
        _mm_storeu_si128((__m128i *) plain, ctr+rand());
        aes128_enc(key_schedule, plain, cipher);
        keyExp1 = *(uint64_t *)cipher;
        keyExp1 = keyExp1 & 0xFFFF;
        // std::cout << "keyExp: " << std::bitset<16>(keyExp1) << std::bitset<64>(keyExp0) << std::endl;

        /*
            the initialization process of the Grain v1 cipher
        */
        state.initialization(keyExp0, keyExp1, ivExp);

        /*
            selection of the 10 bits that correspond to two successive input of $h$
        */
        int current = selectTargetBit(state) << 5;
        state.clock(false);
        current ^= selectTargetBit(state);

        /*
            stocking the generated data
        */
        configuration[current]++;
    }

    /*
        we stock this output minus the first line and use it as an input of the plotter.py to generate figure 5 of the paper.
    */
    std::cout << "after " << nbOfExp << " experiences, this is how many times each input of h was obtained" << std::endl;
    for(int i = 0; i < (1<<10); i++) {
        std::cout << "      value " << std::bitset<10>(i) << " was reached " << configuration[i] << " times" << std::endl;
    }

}


int dichotomy(std::vector<uint>& indexMax, std::vector<uint32_t> const& configuration, uint32_t value) {
    int first = 0, last = indexMax.size()-1, result = 0;
    while(first < last) {
        result = (first + last)/2;
        if(configuration[indexMax[result]] == value) {
            return result;
        } else if (configuration[indexMax[result]] > value) {
            first = result + 1;
        } else {
            last = result - 1;
        }
    }
    return result;
}

void newExp(unsigned numberOfSampling, bool moreTurn) {
    /*
        generation of master key for randomness generation
    */
    std::srand(time(0));
    uint8_t masterKey[16];
    for(int i = 0; i < 16; i++) masterKey[i] = rand();
    __m128i key_schedule[20];
    aes128_load_key(masterKey, key_schedule);

    /*
        generation of 3/4 random keystream values
    */

    // 0x7 for 3 bits of keystream
    // 0xF for 4 bits of keystream

    /*
        randomly generate r < 2^20 to genertae keystream after r rounds
    */
    uint32_t r = rand() & 0x3FF;


    // to store the internal states reached
    std::vector<uint32_t> configuration(1<<15, 0);

    /*
        the experiences in itself;
    */
    #pragma omp parallel for
    for(unsigned samplingNum = 0; samplingNum < numberOfSampling; samplingNum++) {
        uint64_t keyExp0, keyExp1, ivExp;
        __m128i ctr = {0, 0};
        uint8_t plain[16], cipher[16];
        PartialState state = PartialState();
        uint64_t partialValue = 0;


        _mm_storeu_si128((__m128i *) plain, ctr+=samplingNum);
        aes128_enc(key_schedule, plain, cipher);
        keyExp0 = *(uint64_t *)cipher;
        ivExp = (*(uint64_t *)(cipher + 8));
        // std::cout << "ivExp: " << std::bitset<64>(ivExp) << std::endl;
        _mm_storeu_si128((__m128i *) plain, ctr+=samplingNum+1);
        aes128_enc(key_schedule, plain, cipher);
        keyExp1 = *(uint64_t *)cipher;
        keyExp1 = keyExp1 & 0xFFFF;
        // std::cout << "keyExp: " << std::bitset<16>(keyExp1) << std::bitset<64>(keyExp0) << std::endl;
        /*
            compute the partial state from the key and frame
        */
        state = PartialState();
        state.initialization(keyExp0, keyExp1, ivExp);
        /*
            sample r randomly and do r turn before observing the generated keystream
        */
        if(moreTurn) for(uint32_t i = 0; i < r; i++) state.clock(false);
        partialValue ^= (selectTargetBit(state));
        state.clock(false);
        partialValue ^= ((selectTargetBit(state)) << 5);
        state.clock(false);
        partialValue ^= ((selectTargetBit(state)) << 10);

        // only for 4 bits of keystream
        // state.clock(false);
        // partialValue ^= ((selectTargetBit(state)) << 15);

        #pragma omp critical
        {
          configuration[partialValue] += 1;
        }
    }

    double prob = 0;

    /*
        maximising the probability at the end
    */

    map<uint32_t, uint32_t> mymap;
    for (uint32_t i = 0; i < 1u << 15; ++i) mymap[configuration[i]] += 1;

    //for (auto const & p : mymap) cout << p.first << ": " << p.second << endl;

    auto rit = mymap.rbegin();
    uint32_t nb_touch = 0;
    uint32_t size_X = 0;
    while (size_X < 18820) { // take the set of 2^{14.2} values reached the most
      if (rit->second == 0) ++rit;
      else {
        nb_touch += rit->first;
        rit->second -= 1;
        size_X += 1;
      }
    }
    prob = nb_touch;
    prob /= numberOfSampling;



    std::cout << " The computed probability is " << prob << std::endl;
}
