#ifndef _EXPERIENCE_HPP_
#define _EXPERIENCE_HPP_

#include "partialstate.hpp"

/*
    function to select the input bits of h from the current state
*/
int selectTargetBit(PartialState state);

int selectTargetBitExtend(PartialState state);

int dichotomy(std::vector<uint>& indexMax, std::vector<uint32_t> const& configuration, uint32_t value);

/*
    function to run the experience describe in Section 4.3, Algorithm 5
*/
void exp(uint64_t nbOfExp);

void newExp(unsigned numberOfSampling, bool moreTurn);

#endif
