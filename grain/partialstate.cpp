#include "partialstate.hpp"

PartialState::PartialState() : lfsr0(0LL), lfsr1(0LL), nfsr0(0LL), nfsr1(0LL) {}

void PartialState::printPartialState() {
    std::cout << "lfsr: ";
    std::cout << std::bitset<16>(lfsr1) << std::bitset<64>(lfsr0);
    std::cout << std::endl << "nfsr: ";
    std::cout << std::bitset<16>(nfsr1) << std::bitset<64>(nfsr0);
    std::cout << std::endl;
}

uint64_t PartialState::computeH() {
    return ((lfsr0 >> 25) ^ (nfsr0 >> 63) ^ ((lfsr0 >> 3) & (lfsr1)) ^ ((lfsr0 >> 46) & (lfsr1)) ^ ((lfsr1) & (nfsr0 >> 63))
        ^ ((lfsr0 >> 3) & (lfsr0 >> 25) & (lfsr0 >> 46)) ^ ((lfsr0 >> 3) & (lfsr0 >> 46) & (lfsr1))
        ^ ((lfsr0 >> 3) & (lfsr0 >> 46) & (nfsr0 >> 63)) ^ ((lfsr0 >> 25) & (lfsr0 >> 46) & (nfsr0 >> 63))
        ^ ((lfsr0 >> 46) & (lfsr1) & (nfsr0 >> 63))) & 1LL;
}

uint64_t PartialState::computeZ() {
    uint64_t result = computeH();
    return (result ^ (nfsr0 >>  1) ^ (nfsr0 >>  2) ^ (nfsr0 >>  4) ^ (nfsr0 >>  10) ^ (nfsr0 >>  31) ^ (nfsr0 >>  43) ^ (nfsr0 >>  56)) & 1;
}

void PartialState::load(uint64_t key0, uint64_t key1, uint64_t iv){
    lfsr0 = iv;
    lfsr1 = 0xFFFF;
    nfsr0 = key0;
    nfsr1 = key1 & 0xFFFF;
}

void PartialState::clock(bool init){
    uint64_t carryLFSR = lfsr1 & 1;
    uint64_t carryNFSR = nfsr1 & 1;
    uint64_t output = (computeH() ^ (nfsr0 >> 1) ^ (nfsr0 >> 2) ^ (nfsr0 >> 4) ^ (nfsr0 >> 10) ^ (nfsr0 >> 31) ^ (nfsr0 >> 43) ^ (nfsr0 >> 56)) & 1LL;

    uint64_t feedbackLFSR = ((lfsr0) ^ (lfsr0 >> 13) ^ (lfsr0 >> 23) ^ (lfsr0 >> 38) ^ (lfsr0 >> 51) ^ (lfsr0 >> 62)) & 1LL;
    uint64_t feedbackNFSR = ((lfsr0) ^ (nfsr0 >> 62) ^ (nfsr0 >> 60) ^ (nfsr0 >> 52) ^ (nfsr0 >> 45) ^ (nfsr0 >> 37)
        ^ (nfsr0 >> 33) ^ (nfsr0 >> 28) ^ (nfsr0 >> 21) ^ (nfsr0 >> 14) ^ (nfsr0 >> 9) ^ (nfsr0)
        ^ ((nfsr0 >> 63) & (nfsr0 >> 60)) ^ ((nfsr0 >> 37) & (nfsr0 >> 33)) ^ ((nfsr0 >> 15) & (nfsr0 >> 9))
        ^ ((nfsr0 >> 60) & (nfsr0 >> 52) & (nfsr0 >> 45)) ^ ((nfsr0 >> 33) & (nfsr0 >> 28) & (nfsr0 >> 21))
        ^ ((nfsr0 >> 63) & (nfsr0 >> 45) & (nfsr0 >> 28) & (nfsr0 >> 9)) ^ ((nfsr0 >> 60) & (nfsr0 >> 52) & (nfsr0 >> 37) & (nfsr0 >> 33)) ^ ((nfsr0 >> 63) & (nfsr0 >> 60) & (nfsr0 >> 21) & (nfsr0 >> 15))
        ^ ((nfsr0 >> 63) & (nfsr0 >> 60) & (nfsr0 >> 52) & (nfsr0 >> 45) & (nfsr0 >> 37)) ^ ((nfsr0 >> 33) & (nfsr0 >> 28) & (nfsr0 >> 21) & (nfsr0 >> 15) & (nfsr0 >> 9))
        ^ ((nfsr0 >> 52) & (nfsr0 >> 45) & (nfsr0 >> 37) & (nfsr0 >> 33) & (nfsr0 >> 28) & (nfsr0 >> 21))
    ) & 1LL;


    if(init) {
        feedbackLFSR ^= output;
        feedbackNFSR ^= output;
    }

    /* the shift of all fsr */
    lfsr0 = (lfsr0 >> 1) ^ (carryLFSR << 63);
    lfsr1 = (lfsr1 >> 1) ^ (feedbackLFSR << 15);

    nfsr0 = (nfsr0 >> 1) ^ (carryNFSR << 63);
    nfsr1 = (nfsr1 >> 1) ^ (feedbackNFSR << 15);
}

void PartialState::initialization(uint64_t key0, uint64_t key1, uint64_t iv) {
    load(key0, key1, iv);
    // printPartialState();

    for(int i = 0; i < 160; i++) {
        clock(true);
    }

}
