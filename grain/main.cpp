#include "experience.hpp"

int main(int argc, char *argv[]) {
    // run the experiences; the output is directly print in ther terminal
    // exp(1 << 10);

    /*
        the new expereiences
    */
    // for(int i = 1; i <= 10; i++) {
    //     std::cout << "experiment " << i <<": " << std::endl;
    //     newExp(16777216, false); // 2^24
    //     std::cout << "------------------------------------------------------------------------------" << std::endl << std::endl;
    // }

    /*
        the new experience with r turns before concidering the keystream generation
    */
    for(uint i = 0; i < 10u; i++) {
        std::cout << "experiment " << i << ": " << std::endl;
        newExp(1u << 26, true);
        std::cout << "------------------------------------------------------------------------------" << std::endl << std::endl;
    }
}
