#ifndef _PARTIALSTATE_HPP_
#define _PARTIALSTATE_HPP_

#include <iostream>
#include <bitset>
#include <vector>
#include <stdint.h>     //for int8_t
#include <string.h>     //for memcmp
#include <wmmintrin.h>

/* grain v1 */

class PartialState {
public:
    //attributs
    uint64_t lfsr0; //part of the lfsr corresponding to register s_{79} (being the weakest weight bit) to s_{16}
    uint64_t lfsr1; //part of the lfsr corresponding to register s_{15} (being the weakest weight bit) to s_{0}
    uint64_t nfsr0; //part of the lfsr corresponding to register b_{79} (being the weakest weight bit) to b_{16}
    uint64_t nfsr1; //part of the lfsr corresponding to register b_{15} (being the weakest weight bit) to b_{0}

    //constructors
    PartialState();

    //other methods
    /*
        to print the grain internal state in the terminal
    */
    void printPartialState();

    /*
        compute the output of the function $h$ for the current internal state; (it is not the output)
    */
    uint64_t computeH();

    uint64_t computeZ(); 

    /*
        method to load the key and iv value during a Grain v1 initialization phase
    */
    void load(uint64_t key0, uint64_t key1, uint64_t iv);

    /*
        one clocking of the internal state
        the input control the xoring of the output during the initialization
    */
    void clock(bool init);

    /*
        the full initialization procedure of Grain v1
    */
    void initialization(uint64_t key0, uint64_t key1, uint64_t iv);
};

#endif
